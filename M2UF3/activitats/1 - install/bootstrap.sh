#!/bin/sh

# Configuració locales
sed -i s/"# en_US.UTF-8 UTF-8"/"en_US.UTF-8 UTF-8"/ /etc/locale.gen
sed -i s/"# es_ES.UTF-8 UTF-8"/"es_ES.UTF-8 UTF-8"/ /etc/locale.gen
sed -i s/"# ca_ES.UTF-8 UTF-8"/"ca_ES.UTF-8 UTF-8"/ /etc/locale.gen
locale-gen
